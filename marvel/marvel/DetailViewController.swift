//
//  DetailViewController.swift
//  marvel
//
//  Created by Brayan Jimenez on 3/1/18.
//  Copyright © 2018 Brayan Jimenez. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class DetailViewController: UIViewController {
    
    @IBOutlet weak var pokemonImageView: UIImageView!
    var pokemon:Pokemon?
   

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = pokemon?.name
        let pkService = PokemonService()
        pkService.getPokemonImage(id: (pokemon?.id)!){ (pkImage) in
            self.pokemonImageView.image = pkImage
        }
    }
    
    
    
  

}
